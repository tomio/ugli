class ProductsController < ApplicationController
	skip_before_action :verify_authenticity_token, :only => [:delete_image, :delete_background_image, :add_to_cart, :get_cart_items, :remove_cart_item]

	before_action :check_buyer_params, :only =>[:confirm]


	include ActionView::Helpers::NumberHelper
	def index

		@landing_title = "" 

		if TitleContent.all.count > 0 
			@landing_title = TitleContent.last.landing_collection_title
		end 
		if(cookies[:is_admin])
			@products = Product.where(is_deleted: false, is_front_page: true).order(hidden: :desc, is_members_only: :desc)
		else
			@products = Product.where(is_admin: false, is_deleted: false, is_front_page: true ).order(hidden: :desc, is_members_only: :desc)
		end

		@bg_images = BackgroundImage.all


	end



	def login
	end

	def content 
		check_is_admin()
		if TitleContent.all.count > 0 
			@content = TitleContent.last 
		else 
			@content = TitleContent.new 
		end 

		@bg_images = BackgroundImage.all
	end

	def update_content
		check_is_admin()
		landing_title = params["landing_title"]
		shop_title = params["shop_title"]	
		image = params["images"]

		if TitleContent.all.count > 0 
			content = TitleContent.last 
			content.update_attribute(:landing_collection_title, landing_title)
			content.update_attribute(:shop_collection_title, shop_title)
		else 
			TitleContent.create(:landing_collection_title => landing_title, :shop_collection_title => shop_title)
		end 


		if image != nil
			bg_img = BackgroundImage.create()
			bg_img.image.attach(image)
		end

		redirect_to "/content"

	end 

	def launch
	end

	def auth_user

		email = params["email"]
		password = params["password"]

		if(email == "tomi@uglimyclothes.com" && password == "trussbruv")
			cookies[:is_admin] = true
			redirect_to "/stock"
		else
			redirect_to "/"
		end

	end

	def change_background_video
		check_is_admin()
		@videos = BackgroundVideo.where(:is_current => true)
	end

	def update_background_video
		BackgroundVideo.where(:is_current => true).update_all(:is_current => false)
		background_video = BackgroundVideo.new
		mobile_video = params["mobile_video"]
		desktop_video = params["desktop_video"]
		background_video.videos.attach(desktop_video)
		background_video.videos.attach(mobile_video)
		background_video.update_attribute(:is_current,true)
		background_video.save
		redirect_to "/products"


	end

	def get_background_video

		device = params["device"]
		video = BackgroundVideo.where(is_current: true).first
		if device == "desktop"
			video = video.videos.first
		elsif device == "mobile"
			video = video.videos.last
		end

		render status: 200, json:
		{
			video: url_for(video)
		}.to_json

	end

	def all
		@shop_title = "" 

		if TitleContent.all.count > 0 
			@shop_title = TitleContent.last.shop_collection_title
		end 

		if(cookies[:is_admin])
			@products = Product.where(is_deleted: false).order(:rank, hidden: :desc, is_members_only: :desc)
		else
			@products = Product.where(is_admin: false, is_deleted: false).order(:rank, hidden: :desc, is_members_only: :desc)
		end
	end

	def show
		@product = Product.find(params[:id])

		if((@product.is_admin && !cookies[:is_admin]) || @product.hidden)
			redirect_to "/"
		end
		
		# @sizes = @product.avail_sizes
		@sizes = ["S","M","L"]


	end

	def add_to_cart

		if session[:cart] == nil || session[:cart].count == 0
			session[:cart] = {}
			session[:start] = Time.now
		end

		if !session[:cart].has_key?(params[:product_id])
			product = Product.find(params[:product_id])
			session[:cart][product.id] = {:size => params[:size]}

			session[:total] = session[:total].to_f + product.price
			render status: 200, json:
			{
				added: true,
				cart_length: session[:cart].count,
				product: product,
				image: url_for(product.images.order(:order_num).first),
				size: params[:size],
				total: session[:total]

			}.to_json
		else
			render status: 200, json:
			{
				added: false,
				message: "Due to high demand this can only be bought once"

			}.to_json

		end

	end

	def remove_cart_item
		session[:cart].delete(params[:product_id])
		product = Product.find(params[:product_id])

		session[:total] = session[:total].to_f - product.price

		render status: 200, json:
		{

			cart_length:session[:cart].count,
			total: session[:total]

		}.to_json

	end

	def get_size_info

		product_id = params[:product_id]
		size = params[:size]

		sold_out = true 

		stock = Stock.where( product_id: product_id, size: size )

		if stock.count > 0	 
			sold_out = !(stock.first.amount > 0)
		end 

		render status: 200, json:
		{
			sold_out: sold_out
		}.to_json

	end 

	def get_cart_items

		if session[:cart] != nil
			if session[:start] == nil || session[:start] < Time.now - 60 * 30
				clear_user_cart_info()
			else
				session[:start] =  Time.now
			end
		end

		cart = session[:cart]
		products = []
		active_storage_disk_service = ActiveStorage::Service::DiskService.new(root: Rails.root.to_s + '/storage/')
		total = 0
		if cart != nil
			cart.each do |product_id, cartItem|
				size = cartItem["size"]

				product = Product.find(product_id)

				total +=  product.price
				products.push({
							product: product,
							image: url_for(product.images.order(:order_num).first),
							size: size
						})
			end

		end

		session[:total] = total


		render status: 200, json:
		{

			products: products,
			total: total

		}.to_json


	end


	def begin_review
		

	end

	def new
		check_is_admin()
	end


	def create
		check_is_admin()

		@product = Product.new(product_params)
		if @product.save
			redirect_to "/stock"
		else
			@messages = @product.errors
			render action: :new
		end

	end
	def get_all_stock

		check_is_admin()
		@products = Product.all
	end

	def get_stock
		check_is_admin()

		@product = Product.find(params[:product_id])
		@stock = Stock.new

	end


	def update_stock
		check_is_admin()
		amount = params[:amount]
		size = params[:size]
		name = params[:name]
		style = params[:style]
		price = params[:price]
		link = params[:link]
		rank = params[:rank]

		product = Product.find(params[:product_id])

		is_admin = params.has_key?("is_admin")
		is_deleted = params.has_key?("is_deleted")
		is_members_only = params.has_key?("is_members_only")
		is_hidden = params.has_key?("hidden")
		is_front_page = params.has_key?("is_front_page")
		is_linked = params.has_key?("is_linked")


		description =  params[:description]
		product.update_attribute(:price, price)
		product.update_attribute(:name, name)
		product.update_attribute(:name, name)
		product.update_attribute(:style, style)
		product.update_attribute(:link, link)
		product.update_attribute(:rank, rank)

		product.update_attribute(:is_admin, is_admin)
		product.update_attribute(:is_deleted, is_deleted)
		product.update_attribute(:description, description)
		product.update_attribute(:is_members_only, is_members_only)
		product.update_attribute(:hidden, is_hidden)
		product.update_attribute(:is_front_page, is_front_page)
		product.update_attribute(:is_linked, is_linked)


		images = product.images
		new_images = params["images"]

		images.each do |image|
			new_image_order = params["image-"+image.id.to_s]
			image.update_attribute(:order_num, new_image_order)

		end


		if new_images != nil
			product.images.attach(new_images)
		end

		existing_stock = product.stocks.where(size: size).first

		if existing_stock != nil
			existing_stock.update_attribute(:amount, amount)
		else
			size_val = 0
			case size
			when "S"
			  size_val = 1
			when "M"
			  size_val = 2
			when "L"
				size_val = 3
			when "XL"
				size_val = 4
			end

			#if the user does not update the sizes
			if size_val > 0
				Stock.create(:size => size, :amount => amount, :product_id => product.id, :size_value => size_val)
			end

		end

		redirect_to "/stock/"+product.id.to_s


	end

	def review
		check_is_admin()

		@products = []
		session[:cart].each do |product_id, cart_item|

			product = Product.find(product_id)
			@products.push({:product =>  product, :size => cart_item["size"]

				})

		end

		if cookies[:member_id]
			@member = Member.find(cookies[:member_id])
		end


	end

	def confirm
	 	shipping = Ugli::SHIPPING_COST
		
		if cookies[:member_id]
			member.update_attribute(:address, session[:address])
			member.update_attribute(:state, session[:state])
			member.update_attribute(:zip, session[:zip])
			member.update_attribute(:city, session[:city])
			member.update_attribute(:name, session[:name])
		end

		# Stripe.api_key = "sk_test_WClc2JMNTyOv6wqCVqLrpvcL00l7JKuoYX"
		Stripe.api_key = "sk_live_Kw73lufNyf4XIRsE5UeqPkD000XISDZZlz"

		tax_cost = (session[:total].to_f * Ugli::STATE_TAX_COST).round(2)

		price = (( tax_cost + shipping + session[:total].to_f).round(2) * 100).to_i

		card_string = params[:brand] + " **** " + params[:last4]

		charge = Stripe::Charge.create({
		    amount: price,
		    currency: 'usd',
		    description: 'Ugli Clothing Charge',
		    source: params[:stripeToken],
		})

		buyer_address = params[:buyer_address]

		if params[:buyer_apt].length > 0 
			buyer_address = buyer_address + " Unit: " + params[:buyer_apt]
		end 

	if charge[:paid]
		if cookies[:member_id]
			order = Order.create(:name => params[:buyer_name],
						:email => params[:buyer_email],
						:address => buyer_address,
						:state => params[:buyer_state],
						:city => params[:buyer_city],
						:card => card_string,
						:zip => params[:buyer_zip],
						:items => session[:cart],
						:member_id => cookies[:member_id]

						)
		else
			order = Order.create(:name => params[:buyer_name],
						:email => params[:buyer_email],
						:address => buyer_address,
						:state => params[:buyer_state],
						:city => params[:buyer_city],
						:card => card_string,
						:zip => params[:buyer_zip],
						:items => session[:cart],
						)

		end

			detract_stock()

			UserMailer.purchase_email(
									:name => params[:buyer_name],
									:email => params[:buyer_email],
									:address => buyer_address,
									:state => params[:buyer_state],
									:city => params[:buyer_city],
									:card => card_string,
									:zip => params[:buyer_zip],
									:card => card_string,
									:shipping => shipping,
									:tax_cost => tax_cost, 
									:total => session[:total],
									:order_num => order.order_num,
									cart: session[:cart],
								).deliver_now
				
			UserMailer.new_order_email(
				:customer_name => params[:buyer_name],
				:order_date => order.created_at.strftime("%m/%d/%Y %T"), 
				:order_total => (tax_cost + shipping + session[:total].to_f).round(2)).deliver_now
				
			clear_user_cart_info
			redirect_to "/thanks"
		else
			session[:payment_error] = "The payment was unsuccesful. Please try another payment method"
			redirect_to "/checkout"
		end 
	end

	def thanks
	end

	def pending_orders
		check_is_admin()
		@orders = Order.all.order(:created_at)
	end

	def show_pending_order
		check_is_admin()

		@order = Order.find(params[:order_id])

		 	@products = []

		@order.items.each do |product_id, item|
			product = Product.find(product_id)
			@products.push({:product =>  product, :size => item["size"]})
		end

		@name = @order.name
  	@full_address = @order.address + ", "+@order.city+ ", "+@order.state + ", "+ @order.zip
	end

	def update_tracking_number
		check_is_admin()

		order = Order.find(params[:order_id])
		order.update_attribute(:tracking_number,params[:tracking_number])
		order.update_attribute(:shipping_method,params[:shipping_method])
		UserMailer.shipped_email(order_id: order.id).deliver_now
		redirect_to "/pending_orders"

	end

	def checkout

		if session[:cart] == nil || !(session[:cart].count > 0) 
			redirect_to "/all"
		end 
		
		if cookies[:member_id]
			member = Member.find(cookies[:member_id])
			session[:name] = member.name
			session[:address] = member.address
			session[:city] = member.city
			session[:state] = member.state
			session[:zip] = member.zip
			session[:email] = member.email

		end
		@shipping = Ugli::SHIPPING_COST
		@tax_cost = (Ugli::STATE_TAX_COST * session[:total].to_f).round(2)

		@complete_total = session[:total].to_f + @shipping + @tax_cost


	end

	def contact
	end

	def submit_contact

		email = params[:email]
		name = params[:name]
		subject = params[:subject]
		message = params[:message]
		UserMailer.contact_email(name: name, email: email, subject: subject, message: message).deliver_now

		redirect_to "/message_received"


	end

	def after_contact
	end

	def about
	end 
	
	def return 
	end 

	def delete_image
		check_is_admin()
		product = Product.find(params[:product_id])
		product.images.find(params[:image_id]).destroy
	end

	def delete_background_image
		check_is_admin()
		if BackgroundImage.all.count > 0 
			image = BackgroundImage.find(params[:image_id]).destroy
			render status: 200, json:
			{
				deleted: true,
				message: "Image Deleted Successfully"
			}.to_json

		else 
			render status: 400, json:
			{
				added: false,
				message: "There must be at least one image left for the background"
			}.to_json

		end 
	end


	private

	REGEX_PATTERN = /^(.+)@(.+)$/ 
	def is_email_valid? email
		email =~REGEX_PATTERN
	end

	def check_buyer_params
		session[:name] = params[:buyer_name]
		session[:email] = params[:buyer_email]
		session[:address] = params[:buyer_address]
		session[:state] = params[:buyer_state]
		session[:city] =  params[:buyer_city]
		session[:zip] = params[:buyer_zip]
		session[:apt] = params[:buyer_apt]

		if params[:buyer_name] == '' || params[:buyer_address] == '' || params[:buyer_city] == '' ||
			params[:buyer_state] == '' || params[:buyer_zip] == '' || params[:buyer_email] == ''

			session[:address_error] = "Please make sure you fill out all the fields"
			redirect_to "/checkout"
		elsif !is_email_valid?(params[:buyer_email])
			
			session[:address_error] = "Please enter in valid email"
			redirect_to "/checkout"

			# abort (params[:buyer_name]).inspect
		
		end
	end 

	def detract_stock 
		session[:cart].each do |product_id, cart_item|
			product = Product.find(product_id)
			existing_stock = product.stocks.where(size: cart_item["size"]).first 
			current_amount = existing_stock.amount 
			existing_stock.update_attribute(:amount, current_amount - 1)
		end
	end 


	def clear_user_cart_info
		session.delete(:cart)
	end 

	def product_params
		params.require(:product).permit(:name,:description,:price,images: [])
	end

	def check_is_admin
		if(!cookies[:is_admin] == true)
			redirect_to "/launch"
		end
	end

end
