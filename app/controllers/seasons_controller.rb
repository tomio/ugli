class SeasonsController < ApplicationController

    def index
        @seasons = Season.all
    end
    
	def new
	end 

	
	def lookbook
		@seasons = Season.all

	end
	
	def new_collection
		@season = Season.find(params[:id])

	end 

	def save_collection
		@season = Season.find(params[:season_id])
		@collection = Collection.new(name: params[:collection][:name], season_id: params[:season_id])
		 

		if @collection.save
			redirect_to "/seasons/"+@season.id.to_s
		else
			@messages = @collection.errors
			# render action: :new_collection
		end

	end 

	def save_collection_item
		@season = Season.find(params[:season_id])
		# @collection = Collection.find(params[:collection_id])
		# abort params.inspect
		@collection_item = CollectionItem.new(collection_item_params)

		if @collection_item.save
			redirect_to "/seasons/"+@season.id.to_s+"/collections/"+@collection.id.to_s
		else
			@messages = @collection.errors
			# render action: :new_collection
		end

	end 

    def create

		@season = Season.new(season_params)
		if @season.save
			redirect_to "/seasons"
		else
			@messages = @season.errors
			render action: :new
		end

    end

    def show
		@season = Season.find(params[:id])
		@collections = @season.collections 
	end

	def show_collection
		@season = Season.find(params[:id])
		@collection = Collection.find(params[:collection_id])
	end

	def new_collection_item
		@season = Season.find(params[:id])
		@collection = Collection.find(params[:collection_id])
	end

    private 

    def season_params
		params.require(:season).permit(:name)
	end

	def collection_params
		params.require(:collection).permit(:name, :season_id)
	end


	def collection_item_params
		params.require(:collection_item).permit(:collection_id, :photo)
	end

end 