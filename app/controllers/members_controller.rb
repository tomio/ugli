class MembersController < ApplicationController
  def show
		@member = Member.find(params[:id])
	end

	def new
		@member = Member.new
	end

	def create
		@member = Member.new(member_params)
		if @member.save
			cookies[:member_id] = @member.id
			redirect_to "/"
		else
			flash[:error] = "#{@member.errors.details}  "
			render action: :new
		end
	end

	def edit

		@member = Member.find_by_id(params[:id])

		if params[:id].to_i == session[:member].to_i
			@member = Member.find_by_id(params[:id])
		else
			redirect_to "/videos"
		end
	end

	def raffle 
		if cookies.has_key?(:member_id)
			@member = Member.find(cookies[:member_id])
		end 
	end 

	def enter_raffle

		member = Member.find(cookies[:member_id])
		ig = params[:instagram]
		size = params[:size]
		member.update_attribute(:instagram,ig)
		member.update_attribute(:size,size)
		redirect_to "/raffle"




	end 

	def logout
    cookies.delete :member_id
    cookies.delete :is_admin
		redirect_to "/"

	end

	def member_list 
		check_is_admin()
		@members = Member.all 
	end 

	def login
		@member = Member.new
		@error = false
		if params.key?("email") and params.key?("password")
      if(params[:email] == "tomi@uglimyclothes.com" && params[:password] == "trussbruv")
        cookies[:is_admin] = true
        redirect_to "/stock"
      else
  			@member = Member.find_by(email: params[:email])
  			if @member and @member.authenticate( params[:password])
          cookies[:member_id] = { value: @member.id, expires: 1.hour }
  			  redirect_to "/"
  			else
  				@member = Member.new
  				@member.email = params[:email]
  				@error = true
  				render action: :login
  			end
		  end
    end
	end

	private

	def check_is_admin
		if(!cookies[:is_admin] == true)
			redirect_to "/"
		end
	end

	def member_params
		params.require(:member).permit(:email,:password)

	end

end
