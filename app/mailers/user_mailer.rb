class UserMailer < ApplicationMailer


  def purchase_email(email: "",cart: [], state: "", city: "", shipping: "",
  					 card: "" , zip: "", address: "", name: "", subtotal: "", total: "",order_num: "", tax_cost: "")
  	@email = email
  	@name = name
  	@card = card
  	@total = total
  	@cart = cart
  	@shipping = shipping
    @order_num = order_num
	@tax_cost = tax_cost


	@products = []
	cart.each do |product_id, cart_item|

		product = Product.find(product_id)
		@products.push({:product =>  product, :size => cart_item["size"]})

	end

  	@full_address = address + ", "+city+ ", "+state +", "+ zip
    mail(to: @email, subject: 'Thanks for purchasing from Ugli')
  end

  def shipped_email(order_id: "")
  	@order = Order.find(order_id)

  	@products = []

		@order.items.each do |product_id, item|
			product = Product.find(product_id)
			@products.push({:product =>  product, :size => item["size"]})
		end

		@name = @order.name
  	@full_address = @order.address + ", "+@order.city+ ", "+@order.state + ", "+ @order.zip
		mail(to: @order.email)


  end

  def contact_email(name: "", email: "",subject: "",message: "")
  	@name = name
  	@message = message
  	mail(to: "tomi@uglimyclothes.com", reply_to: email,
  			 from: email,  subject: subject)

  end

	def new_order_email(customer_name: "", order_date: "",order_total: "")
		@customer_name = customer_name
		@order_date = order_date
		@order_total = order_total

		mail(to: ["dogol@uglimyclothes.com","tomi@uglimyclothes.com","tomiokiji@gmail.com","dogol2015@gmail.com"],
					from: "tomi@uglimyclothes.com",  subject: "New Product was Purchased")

	end




end
