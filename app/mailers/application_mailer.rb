class ApplicationMailer < ActionMailer::Base
  default from: 'tomi@uglimyclothes.com'
  layout 'mailer'
end
