class Collection < ApplicationRecord
    belongs_to :season
    has_many :collection_items
end
