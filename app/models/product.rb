class Product < ApplicationRecord
	has_many_attached :images
	has_many :stocks


	def avail_sizes
		sizes = []
		stock_sizes = self.stocks.order(:size_value).select(:size)

		stock_sizes.each do |stock|

			if !sizes.include?(stock.size)
				sizes.push(stock.size)

			end;

		end

		return sizes

	end

	def stock_info()
		stocks = self.stocks.order(:size_value)
		stock_info = []

		stocks.each do |stock|
			stock_info.push([stock.size,stock.amount])
		end

		return stock_info
	end

	def sold_out
		return !(self.stocks.where("amount > ?", 0).count > 0)
	end

end
