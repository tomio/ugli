class Order < ApplicationRecord
	UID_RANGE = ('1'..'9').to_a
	generate_public_uid generator: PublicUid::Generators::RangeString.new(6,UID_RANGE), column: :order_num
	serialize :items
end
