class BackgroundImage < ApplicationRecord
    has_one_attached :image
end
