class Member < ApplicationRecord
  has_secure_password
  validates :password_confirmation, presence: false

end
