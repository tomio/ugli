


	// $("#add_to_cart_button").click(function(e){
    //     debugger; 
	// 	e.preventDefault();
	// 	var productId = $("#product_id").val();
	// 	var productSize = $("#product_size").val();
	// 	submitCartItem(productId,productSize);
	// });

function checkProductSoldOut(){
        let sizeOption = $("#product_size").val();
        let productid =  $( "#product_size" ).attr("data-uid"); 
        
        $.ajax('/get_size_info', {
        method: 'GET',
        data: { size: sizeOption, product_id: productid  }
    }).then(function success(response) {
        if(response.sold_out){
            $("#add_to_cart_button").hide(); 
            $("#sold_out_button").show(); 
        }
        
        else{
            $("#add_to_cart_button").show(); 
            $("#sold_out_button").hide(); 
        }
        });
        
}