
var cartEmpty = null;
var cartOpen = false;


function submitCartItem(productId,productSize){

	$.ajax('/add_to_cart', {
	method: 'POST',
	data:
	{
	    product_id: productId,
	    size: productSize
	}}).then(function success(newItem) {
    	if(newItem.added){
			appendCartItem(newItem);
			$("#total-value").html("$"+parseFloat(newItem.total).toFixed(2));
				openCart();
    	}
    	else{
			showErrorToast(newItem.message);
    	}
 	});

}

function showErrorToast(message) {
		toast = $("#toast-text");
		toast.html(message);
	  	toast.addClass("show");
  		setTimeout(function(){toast.removeClass("show");}, 3000);
}


function openCart() {
  cartOpen = true;
  document.getElementById("cartNav").classList.add("animate-right-show");
  document.getElementById("cartNav").classList.remove("animate-right-hide");
  $("#cartNav").width(300);

}

function closeCart() {
  cartOpen = false;
  document.getElementById("cartNav").classList.add("animate-right-hide");
  document.getElementById("cartNav").classList.remove("animate-right-show");

}

function updateCartInfo(){
	$.ajax('/get_cart_items')
		.then(
				function success(items) {
					if(items.products.length > 0){

						if(cartEmpty != false){
							showCartInfo();
							cartEmpty = false;
						}

						$("#cart-length").html(items.products.length);
						$("#total-value").html("$"+parseFloat(items.total).toFixed(2));
						for(var i = 0; i < items.products.length; i++){
							let item = items.products[i];
							appendCartItem(item);
						}
					}
					else{
						hideCartInfo();
					}


		 });
}

function appendCartItem(item){
		var product = item.product;
		var imagePath = item.image;

		var $itemHolder = $("<div>", { "class": "cart-item-holder"});
		var $line = $("<div>", { "class": "line"});
		$($line).insertAfter("#closeCartButton");
		$($itemHolder).insertAfter("#closeCartButton");
		$productInfo = $("<div>", { "class": "cart-item-info"});
		$itemImage = $("<img>", { "class": "cart-item-image"}).attr("src",imagePath);

		$itemImage.click(function(){
			window.location.href='/products/'+product.id;

		});


		$deleteItemButton = $("<span>", { "class": "delete-cart-item"}).html("&times;");

		$itemHolder.attr('id',"cart-item"+product.id);

		$itemHolder.append($itemImage);
		$itemHolder.append($productInfo);
		var $productNameContainer = $("<div>", { "class": "cart-item-name-container"});

		var $productName = $("<span>", { "class": "cart-item-name"}).html(product.name);
		var $productStyle = $("<span>", { "class": "cart-item-style"}).html(product.style);
		var $productSize = $("<span>", { "class": "cart-item-size"}).html("- "+item.size);
		var $produceCost = $("<span>", { "class": "cart-item-price"}).html("$"+parseFloat(product.price).toFixed(2));



		$productNameContainer.append($productName);
		$productNameContainer.append($deleteItemButton);

		$productInfo.append($productNameContainer);
		$productInfo.append($productStyle);
		$productInfo.append($productSize);
		$productInfo.append($produceCost);

		// $itemHolder.append($deleteItemButton);




		$("#cart-length").html(item.cart_length);

		$deleteItemButton.click(function(){
				removeCartItem(product.id);
			});

		if(cartEmpty != false){
			showCartInfo();
			cartEmpty = false;
		}
}

function removeCartItem(productId){
	$.ajax('/remove_cart_item', {
	method: 'POST',
	data: { product_id: productId }
	})
	.then(

		function success(response) {

			$("#cart-item"+productId).hide(200);

			if(response.cart_length == 0){
				cartEmpty = true;
				hideCartInfo();
			}
			else{

				$("#cart-length").html(response.cart_length);
				$("#total-value").html("$"+parseFloat(response.total).toFixed(2));
			}

	 });

}

function showCartInfo(){
	$("#cart-empty-text").hide();
	$("#total-value").show();
	$("#total-label").show();
	$("#checkout-button").show();
	$("#cart-length").show();
}

function hideCartInfo(){
	$("#total-value").hide();
	$("#total-label").hide();
	$("#checkout-button").hide();
	$("#cart-length").hide();
	$("#cart-empty-text").show();
}
