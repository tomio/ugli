# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def welcome_email
  	UserMailer.purchase_email(email: "tomi@uglimyclothes.com",cart: {2 => {:size => "M"}}, state: "MD", city: "Elkridge",
  					 card: "Visa **** 3242" , zip: "21075", address: "8060 hillrise ct", name: "Tomi Okiji", total: "300", shipping: 4.00)
  end

    def contact_email
    	UserMailer.contact_email(name: "tomi okiji" , email: "tomi@uglimyclothes.com", message:"what it is ",subject: "test email" )
 	end

    def shipped_email
    	UserMailer.shipped_email(order_id: 42)
 	end

	 def new_order_email
    	UserMailer.new_order_email(customer_name: "Drake Aubrey", order_date: "04/26/2020",order_total: "150.00")
 	end

end
