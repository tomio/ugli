# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.paths << "#{Rails.root}/app/assets/videos"

Rails.application.config.assets.precompile += %w(side_menu.css)
Rails.application.config.assets.precompile += %w(toast.css)
Rails.application.config.assets.precompile += %w(purchase.css)
Rails.application.config.assets.precompile += %w(cart_menu.css)
Rails.application.config.assets.precompile += %w(cart.js)
Rails.application.config.assets.precompile += %w(index.css.erb)
Rails.application.config.assets.precompile += %w(login.css) 
Rails.application.config.assets.precompile += %w(all.css.erb) 
Rails.application.config.assets.precompile += %w(launch.css.erb) 
Rails.application.config.assets.precompile += %w(show_product.js)
Rails.application.config.assets.precompile += %w(show.css) 
Rails.application.config.assets.precompile += %w(checkout.css) 
Rails.application.config.assets.precompile += %w(lookbook.css) 

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
