Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

    root 'products#index'


    resources :products
    resources :members
    resources :seasons

    post '/update_content', to: "products#update_content"
    post '/update_stock', to: "products#update_stock"
    post '/begin_checkout', to: "products#begin_checkout"
    post '/review', to: "products#begin_review"
    post '/confirm', to: "products#confirm"
    post '/send_message', to: "products#submit_contact"
    post '/delete_product', to: "products#delete_product"
    post '/delete_image', to: "products#delete_image"
    post '/delete_background_image', to: "products#delete_background_image"

    post '/add_to_cart', to: "products#add_to_cart"
    post '/remove_cart_item', to: "products#remove_cart_item"
    post '/update_tracking_number', to: "products#update_tracking_number"
    post '/update_background_video', to: "products#update_background_video"
		post '/auth_user', to: "products#auth_user"


    post '/login', to: "members#login"
    post '/enter_raffle', to: "members#enter_raffle"

		get '/login', to: "members#login"
    get '/logout', to: "members#logout"
    get '/raffle', to: "members#raffle"

    get '/member_list', to: "members#member_list"
    get '/about', to: "products#about"
    get '/return', to: "products#return"

    get '/content', to: "products#content"
    get '/get_size_info', to: "products#get_size_info"
    get '/get_cart_items', to: "products#get_cart_items"
    get 'stock/:product_id', to: "products#get_stock"
    get 'checkout', to: "products#checkout"
    get 'review', to: "products#review"
    get 'confirmation', to: "products#confirmation"
    get 'thanks', to: "products#thanks"
    get 'all', to: "products#all"
    get '/contact', to: "products#contact"
    get '/message_received', to: "products#after_contact"
    get '/pending_orders', to: "products#pending_orders"
    get '/pending_orders/:order_id', to: "products#show_pending_order"
    get 'stock', to: "products#get_all_stock"
    get '/change_background_video', to: "products#change_background_video"
    get '/get_background_video', to: "products#get_background_video"
    get '/launch', to: "products#launch"

    get 'lookbook', to: "seasons#lookbook"
    get '/seasons/:id/new-collection', to: "seasons#new_collection"
    get '/seasons/:id/collections/:collection_id', to: "seasons#show_collection"
    get '/seasons/:id/collections/:collection_id/new-collection-item', to: "seasons#new_collection_item"
    post '/save-collection', to: "seasons#save_collection"
    post '/save-collection-item', to: "seasons#save_collection_item"


end
