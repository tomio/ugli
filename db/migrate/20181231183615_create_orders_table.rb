class CreateOrdersTable < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :email
      t.string :card 
      t.string :address 
      t.string :city
      t.string :zip 
      t.string :state
      t.text :items 
      t.decimal :price , precision: 5, scale: 2
      t.string :tracking_number 
      t.string :shipping_method 

      t.timestamps


    end
  end
end
