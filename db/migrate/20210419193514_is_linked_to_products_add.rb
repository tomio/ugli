class IsLinkedToProductsAdd < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :is_linked, :boolean, :default => false
  end
end
