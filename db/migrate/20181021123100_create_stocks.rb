class CreateStocks < ActiveRecord::Migration[5.1]
  def change
    create_table :stocks do |t|
      t.string :size
      t.integer :amount
      t.references  :product, index: true, foreign_key: true
      t.timestamps
    end
  end
end
