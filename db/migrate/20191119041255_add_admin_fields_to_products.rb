class AddAdminFieldsToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :is_deleted, :boolean, :default => false
    add_column :products, :is_admin, :boolean, :default => false
    add_column :active_storage_attachments, :is_deleted, :boolean, :default => false
  end
end
