class AddIgToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :instagram, :string
    add_column :members, :size, :string
  end
end
