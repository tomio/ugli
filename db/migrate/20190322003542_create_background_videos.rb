class CreateBackgroundVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :background_videos do |t|
      t.boolean :is_current

      t.timestamps
    end
  end
end
