class AddMembersOnlyToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :is_members_only, :boolean, :default => false
  end
end
