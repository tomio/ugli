class AddOrderToImages < ActiveRecord::Migration[5.2]
  def change
  	add_column :active_storage_attachments, :order_num, :integer,  :default => 0

  end
end
