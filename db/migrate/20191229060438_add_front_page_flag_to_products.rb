class AddFrontPageFlagToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :is_front_page, :boolean, :default => true
  end
end
