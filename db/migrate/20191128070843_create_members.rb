class CreateMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :members do |t|
      t.string :email
      t.string :name
      t.string :address
      t.string :password_digest
      t.timestamps
    end
  end
end
