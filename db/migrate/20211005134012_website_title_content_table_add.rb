class WebsiteTitleContentTableAdd < ActiveRecord::Migration[5.2]
  def change
    create_table :title_contents do |t|
      t.string :landing_collection_title 
      t.string :shop_collection_title 
      t.timestamps
    end
  end
end
