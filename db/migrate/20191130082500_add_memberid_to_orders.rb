class AddMemberidToOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :orders, :member, index: true, foreign_key: true
  end
end
