class LinkStringToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :link, :string, :default => ""
  end
end
