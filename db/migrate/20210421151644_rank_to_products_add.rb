class RankToProductsAdd < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :rank, :integer,  :default => 100
  end
end
