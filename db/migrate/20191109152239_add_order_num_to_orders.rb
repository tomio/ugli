class AddOrderNumToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :order_num, :string
  end
end
