class AddHiddenToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :hidden, :boolean, :default => false
  end
end
